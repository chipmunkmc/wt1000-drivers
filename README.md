# wt1000-drivers

This repository contains drivers for the KMS WT1000 tablet. This tablet is somewhat obscure, and I couldn't find the drivers anywhere. The drivers in this repository were taken from the recovery partition.

If you want the original recovery image, you can get it [here](https://etc.chipmunk.land/Recovery%20Images/KMS%20WT1000.wim) (sha256: `0cad48a56e8194075236be599d25363f48e650e384d762b8d156c922d07d645e`).

# Installation

First of all, run command prompt as administrator, and navigate to the `drivers` folder

For versions of Windows prior to Windows 10, run
```cmd
for /d %i in (*) do pnputil -i -a %i\*.inf
```

For Windows 10 and newer, run
```cmd
for /d %i in (*) do pnputil /add-driver %i\*.inf /install
```

You will be asked to allow for the installation of drivers multiple times, just allow it each time.

This will install all drivers, however, touchscreen firmware will be missing, and as such, touch input won't work. To fix this, simply copy the `SileadTouch.fw` file to `C:\Windows\System32\drivers`.
